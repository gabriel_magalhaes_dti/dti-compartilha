## dti compartilha - Docker + Deploy na AWS

## Environment variables
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_DEFAULT_REGION

## Modificação realizada na AWS
- Adicionada policy "AmazonEC2ContainerRegistryReadOnly" na role "aws-elasticbeanstalk-ec2-role"    