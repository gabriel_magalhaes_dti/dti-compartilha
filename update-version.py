import json
import sys
import os

def updateDockerrun():
	f = 'Dockerrun.aws.json'

	with open(f, 'r') as stream:
		template = json.load(stream)
		template['Image']['Name'] = os.environ.get('REPOSITORY_URL') + ':' + sys.argv[1]

	output = open(f, 'w')
	output.write( json.dumps(template, indent=4) )
	output.close()

updateDockerrun()