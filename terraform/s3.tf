resource "aws_s3_bucket" "version_bucket" {
   bucket = "version-bucket-${var.app_name}"
}

resource "null_resource" "update_version" {
  triggers = { onVersionChange = "${var.app_version}" }
  provisioner "local-exec" {
    command = "python3 update-version.py ${var.app_version}"
  }
}

resource "aws_s3_bucket_object" "upload_version" {
  depends_on = ["null_resource.update_version", "aws_s3_bucket.version_bucket"]
  bucket = aws_s3_bucket.version_bucket.id
  key    = var.app_version
  source = "Dockerrun.aws.json"
}