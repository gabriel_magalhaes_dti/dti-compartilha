resource "aws_elastic_beanstalk_application" "app_eb" {
  name        = "app-${var.app_name}"
  description = "Aplicação ${var.app_name}"
}

resource "aws_elastic_beanstalk_application_version" "version_eb" {
  depends_on = ["aws_s3_bucket_object.upload_version"]
  name        = "version-eb-${var.app_name}-${aws_s3_bucket_object.upload_version.key}"
  application = aws_elastic_beanstalk_application.app_eb.name
  description = "Version ${var.app_version}"
  bucket      = aws_s3_bucket.version_bucket.id
  key         = aws_s3_bucket_object.upload_version.key
}

resource "aws_elastic_beanstalk_environment" "env_eb" {
  depends_on = ["aws_elastic_beanstalk_application_version.version_eb", "aws_iam_instance_profile.eb_profile"]
  name                = "env-${var.app_name}"
  application         = aws_elastic_beanstalk_application.app_eb.name
  solution_stack_name = "64bit Amazon Linux 2 v3.1.2 running Docker"
  version_label       = "${aws_elastic_beanstalk_application_version.version_eb.name}"
  
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "IamInstanceProfile"
    value = "${aws_iam_instance_profile.eb_profile.name}" 
  }
}

output "app_url" { value = "${aws_elastic_beanstalk_environment.env_eb.cname}" }