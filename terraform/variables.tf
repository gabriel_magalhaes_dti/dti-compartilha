provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {
    region = "us-east-1"
  }
}

variable "app_name" { default = "dti-compartilha" }
variable "app_version" { default = "version" }