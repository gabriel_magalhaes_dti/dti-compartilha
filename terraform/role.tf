resource "aws_iam_instance_profile" "eb_profile" {
  name = "eb-profile-${var.app_name}"
  role = "aws-elasticbeanstalk-ec2-role"
}