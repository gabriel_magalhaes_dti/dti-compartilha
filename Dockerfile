# BUILD
FROM node:13.12.0-alpine as build
WORKDIR /app
COPY . ./
RUN yarn install
RUN yarn build

# RUN
FROM nginx:stable-alpine
COPY --from=build /app/build /usr/share/nginx/html
HEALTHCHECK --interval=15s --timeout=4s --retries=3 CMD curl -sI 127.0.0.1 || exit 1
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]